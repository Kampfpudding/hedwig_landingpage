---
title: "Knowledge Extraction"
date: 2019-11-21T12:16:10+01:00
sidebar: false # or false to display the sidebar
sidebarlogo: fresh-white-alt # From (static/images/logo/)
include_footer: true
---

Hedwig extrahiert auf Basis von Open Information Extraction (OIE) Wissen aus deutschsprachigen Texten. OIE bedeutet, dass wir hedwig vorher nicht beibringen müssen, welche Teile eines Satzes welche Bedeutung haben, sondern dass hedwig dies aus der Position im Satz erschließen kann. Deshalb können wir hedwig in vielen Bereichen einsetzen – nicht nur für journalistische Texte.

Aus dem Satz "Der Gießener Kendall Gray (27), ersetzt seit einer Woche Duke Shelton." extrahiert hedwig folgende Tupel:

```
Kendall Gray, par, 27
Kendall Gray, appos, Gießener
Kendall Gray, ersetzen, Duke Shelton | (seit) einer Woche
```

Hedwig erkennt Teile des Satzes, die eine besondere Position einnehmen, etwa Zahlen in Klammern, Substantive vor Entitäten oder verschiedene Präpositionen wie hier "seit". Diese können in Verbindung mit der [Ontologie]({{< ref "ontology-building.md" >}}) eingeordnet und interpretiert werden.
