---
title: "Newsletter"
date: 2019-11-08T16:15:20+01:00
sidebar: false # or false to display the sidebar
sidebarlogo: fresh-white-alt # From (static/images/logo/)
include_footer: true
---

Melde dich zu unserem Newsletter an, erfahre alle zwei Wochen, welche Features wir neu entwickelt haben und gehöre zu den Ersten die sie ausprobieren.

<section>
<script src="/jquery-3.4.1.min.js"></script>
<script>
$(document).ready(function(){

    // Contact form
    $('.newsletter-form').each(function () {
        var $newsletter_form = $(this);
        var $register_button = $newsletter_form.find('.register');

        // Display the hidden form.
        $newsletter_form.removeClass('hidden');
        // Remove the "no javascript" messages
        $('.no-js').detach();

        // Wait for a mouse to move, indicating they are human.
        $('body').mousemove(function () {
            // Unlock the form.
            $register_button.attr('disabled', false);
        });

        // Wait for a touch move event, indicating that they are human.
        $('body').on('touchmove', function () {
            // Unlock the form.
            $register_button.attr('disabled', false);
        });

        // A tab or enter key pressed can also indicate they are human.
        $('body').keydown(function (e) {
            if ((e.keyCode === 9) || (e.keyCode === 13)) {
                // Unlock the form.
                $register_button.attr('disabled', false);
            }
        });
    });
});
</script>

    <div class="content-wrapper">
      <div class="columns">

        <div class="column is-6 is-offset-3" id="form">
        <div class="alert alert-error hidden" id="general-error"><p>Deine E-Mailadresse ist ungültig.</p></div>
        <div class="alert alert-error hidden" id="already-registered"><p>Du bist bereits registriert.</p></div>
        <div class="alert alert-error hidden" id="invalid-mail"><p>Es ist ein Fehler aufgetreten. Hast du eine gültige Mailadresse angegeben?</p></div>
          <form class="newsletter-form" id="newsletter-form" method="POST">
            <div class="columns is-multiline">
              <div class="column is-6">
                <input class="input is-medium" type="text" placeholder="Dein Name" name="name" required>
              </div>
              <div class="column is-6">
                <input class="input is-medium" type="email" placeholder="Deine Mailadresse" name="email" required>
              </div>
              <input type="text" id="edit-url" class="hidden" name="url" placeholder="Skip if you are a human">
              <div class="form-footer has-text-centered mt-10">
                <p><input type="checkbox" name="dsgvo" value="confirmed" required> Ich akzeptiere die <a href="/datenschutz">Datenschutzbestimmungen.</a></p>
                <input type="submit" class="button cta is-large primary-btn raised is-clear register" value="Anmelden" disabled>
              </div>
            </div>
          <div class="alert alert-warning no-js"><p>Bitte aktiviere JavaScript, um das Formular zu nutzen.</p></div>
          </form>
        </div>

        <script id="n2g_script">
        !function(e,t,n,c,r,a,i){e.Newsletter2GoTrackingObject=r,e[r]=e[r]||function(){(e[r].q=e[r].q||[]).push(arguments)},e[r].l=1*new Date,a=t.createElement(n),i=t.getElementsByTagName(n)[0],a.async=1,a.src=c,i.parentNode.insertBefore(a,i)}(window,document,"script","https://static.newsletter2go.com/utils.js","n2g");
        n2g('create', 'v1w7t5xf-kb6pd9lf-1c7p');

        $(function () { 

        $('#newsletter-form').on('submit', function(e) {
        e.preventDefault();
        var recipient = {
          email: $('input[name=email]').val(),
          first_name: $('input[name=name]').val(),
        };

        n2g(
            'subscribe:send', {
            recipient: recipient
        },
		function(data) {
            if (data.status == 201) {
                $('#form').html('<div class="alert alert-success"><p>Danke für deine Anmeldung. Du erhältst in Kürze eine Bestätigungsmail.</p></div>');

            } else if (data.status == 200) {
                if (data.value[0].result.error.recipients.invalid.length) {
                    $('#invalid-mail').removeClass("hidden");
                } else {
                    $('#already-registered').removeClass("hidden");
                }
            } else {
                $('#general-error').removeClass("hidden");
            }
		},
		function(data) {
            $('#general-error').removeClass("hidden");
        }
	);
    });
});
</script>

      </div>
    </div>
  </div>
</section>
