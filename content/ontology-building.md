---
title: "Ontology Building"
date: 2019-11-21T11:34:54+01:00
sidebar: false # or false to display the sidebar
sidebarlogo: fresh-white-alt # From (static/images/logo/)
include_footer: true
---

Eine Ontologie verhält sich zu deinem Interessengebiet wie eine Landkarte zu einer Landschaft. Sie ist ein konstruiertes Modell, das vor allem dazu dient, Probleme zu lösen und Sachverhalte auf eine bestimmte Weise zu beschreiben – so wie sich politische von topographischen Karten unterscheiden, unterscheiden sich auch Ontologien, ganz nach ihrem Verwendungszweck. 

Ganz konkret ist eine Ontologie eine hierarchische Struktur von Klassen. Für ein Basketballteam aus Bamberg könnte die Ontologie etwa so aussehen:

```
owl:Thing
	Entity
            Organisation
            Team
            Bundesland
            Sponsor
            Person
			Rolle
				Geschäftsleitung
					Geschäftsführer
					Sportdirektor
				Spieler
					Center
					Flügelspieler
					Kapitän
					Point Guard
				Trainer
					Cheftrainer
					Coach
					Individualtrainer
					Medi-Coach
```

Die Ontologie wird von Exptert\*innen aus dem jeweiligen Bereich erstellt und dient dazu, das von hedwig extrahierte Wissen besser strukturieren zu können. So kann hedwig zum Beispiel erkennen, dass es sich bei "Brose Bamberg" um ein Team handelt oder bei "Duke Shelton" um einen Center. Da wir durch die Ontolgogie wissen, dass Centers auch immer Spieler sind, können wir ihn leicht in Abfragen die Informationen über alle Spieler integrieren.

