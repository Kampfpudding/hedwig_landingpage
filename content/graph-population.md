---
title: "Graph Population"
date: 2019-11-21T12:41:29+01:00
sidebar: false # or false to display the sidebar
sidebarlogo: fresh-white-alt # From (static/images/logo/)
include_footer: true
---

Die Graphrepräsentation des extrahierten Wissens ist das Herzstück von hedwig. Wie im Artikel über [Knowledge Extraction]({{< ref "knowledge-extraction.md" >}}) gezeigt, extrahiert hedwig in einem ersten Schritt OIE-Tupel aus einem menschenlesbaren Satz. Als nächstes werden diese Tupel in RDF Syntax übersetzt und in einer Datenbank für Graphen abgelegt. 

Von dort können die Informationen strukturiert, ähnlich einer relationalen Datenbank abgefragt werden. Dadurch, dass wir in der Ontologie Regeln aufgestellt haben, wie sich verschiedene Entitäten zueinander verhalten und welche Eigenschaften bestimmte Klassen haben, können wir in diesem Schritt das extrahierte Wissen noch weiter anreichern, und so neues Wissen erzeugen. 

Wer sich für weitere Details interessiert, dem/der sei das zu [hedwig gehörende Paper](https://dl.gi.de/handle/20.500.12116/28994) empfohlen.
